// SAGE2 is available for use under the SAGE2 Software License
//
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
// and University of Hawai'i at Manoa's Laboratory for Advanced Visualization and
// Applications (LAVA)
//
// See full text, terms and conditions in the LICENSE.txt included file
//
// Copyright (c) 2015

"use strict";

var path    = require('path');
var request = require(path.join(module.parent.exports.dirname, 'request'));  // HTTP client request

// API for forecast.io
var apiKey  = "92f925ff56c3a45ab18402d3753b3c88";

function processRequest(wsio, data, config) {
	var headers = {
		'User-Agent':   'NodeJS/5.0',
		'Content-Type': 'application/json'
	};

	// Add the API key
	var url = 'https://api.forecast.io/forecast/' + apiKey + '/';
	// put the latitude and longitude
	url += data.query.location;

	request({url: url, headers: headers}, function(error, response, body) {
		if (!error && response.statusCode == 200) {
			// Parse the reply we got
			var json = JSON.parse(body);
			if (data.broadcast === true) {
				// get the broadcast function from main module
				// send the data to all display nodes
				module.parent.exports.broadcast('broadcast', {app: data.app, func: data.func,
						data: {weather: json, err: null}});
			} else {
				// send data to the master display node
				wsio.emit('broadcast', {app: data.app, func: data.func, data: {weather: json, err: null}});
			}
		}
	});

}

module.exports = processRequest;
