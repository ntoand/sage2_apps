// SAGE2 is available for use under the SAGE2 Software License
//
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
// and University of Hawai'i at Manoa's Laboratory for Advanced Visualization and
// Applications (LAVA)
//
// See full text, terms and conditions in the LICENSE.txt included file
//
// Copyright (c) 2014-2015

/*global google */

function addScript(url, callback) {
	var script = document.createElement( 'script' );
	if( callback ) script.onload = callback;
	script.type = 'text/javascript';
	script.src  = url;
	document.body.appendChild(script);
}

function addCSS( url, callback ) {
	var fileref = document.createElement("link")
    if (callback) fileref.onload = callback;
	fileref.setAttribute("rel", "stylesheet")
	fileref.setAttribute("type", "text/css")
	fileref.setAttribute("href", url)
    document.head.appendChild(fileref);
}


// need a global handler for the callback (i.e. scope pollution)
var gmaps_self;

var gmaps = SAGE2_App.extend( {

	init: function(data) {
		this.SAGE2Init("div", data);
		this.element.id = "div" + this.id;
		this.moveEvents   = "continuous";
		this.resizeEvents = "continuous";
		
		this.map          = null;
		this.div          = null;
		this.lastZoom     = null;
		this.dragging     = null;
		this.position     = null;
		this.scrollAmount = null;
		this.trafficTimer = null;
		this.trafficCB    = null;


		this.div    = document.createElement('div');
		this.div.id = data.id + "_insidediv";
		this.div.style.position = "absolute";
		this.div.style.width    = ui.json_cfg.resolution.width  + "px";
		this.div.style.height   = ui.json_cfg.resolution.height + "px";
		this.element.appendChild(this.div);

		this.lastZoom     = data.date;
		this.dragging     = false;
		this.position     = {x:0, y:0};
		this.scrollAmount = 0;

		this.zoomFactor = null;
		// building up the state object
		this.state.mapType   = null;
		this.state.zoomLevel = null;
		this.state.center    = null;
		this.state.layer     = null;

		// Create a callback function for traffic updates
		this.trafficCB = this.reloadTiles.bind(this);

		// need a global handler for the callback (i.e. scope pollution)
		gmaps_self = this;
		// load CSS to remove overlays
		addCSS(this.resrcPath + "gmaps_style.css", null);
		// load google maps
		addScript('https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=weather&callback=gmaps_self.initialize');

		this.controls.addSlider({
			id: "Zoom",
			begin: 0,
			end: 20,
			increments: 1,
			appHandle: this,
			property: "state.zoomLevel",
			caption: "Zoom",
			labelFormatFunction: function(value, end) {
				return ((value<10)?"0":"") + value + "/" + end;
			}
		});

		var mapLabel =  { "textual":true, "label":"Map", "fill":"rgba(250,250,250,1.0)", "animation":false};
		this.controls.addButton({type:mapLabel, sequenceNo:4, id:"Map"});
		var trafficLabel = { "textual":true, "label":"T", "fill":"rgba(250,250,250,1.0)", "animation":false};
		this.controls.addButton({type:trafficLabel, sequenceNo:5, id:"Traffic"});
		var weatherLabel = { "textual":true, "label":"W", "fill":"rgba(250,250,250,1.0)", "animation":false};
		this.controls.addButton({type:weatherLabel, sequenceNo:3, id: "Weather"});
		this.controls.addButton({type:"zoom-in", sequenceNo:8, id:"ZoomIn"});
		this.controls.addButton({type:"zoom-out", sequenceNo:9, id:"ZoomOut"});
		this.controls.addTextInput({defaultText: "", caption:"Addr", id:"Address"});
		this.controls.finishedAddingControls();
	},

	initialize: function() {
		if (this.state.mapType == null)
			this.state.mapType = google.maps.MapTypeId.HYBRID;
		if (this.state.zoomLevel == null)
			this.state.zoomLevel = 8;
		if (this.state.center == null)
			this.state.center = {lat:41.850033, lng:-87.6500523};
		if (this.state.layer == null)
			this.state.layer = {w:false, t:false};

		// Enable the visual refresh
		google.maps.visualRefresh = true;
		this.geocoder = new google.maps.Geocoder();
		var city = new google.maps.LatLng(this.state.center.lat, this.state.center.lng);
		var mapOptions = {
			center: city,
			zoom: this.state.zoomLevel,
			mapTypeId: this.state.mapType,
			disableDefaultUI: true,
			zoomControl: false,
			scaleControl: false,
			scrollwheel: false
		};
		this.map = new google.maps.Map(this.div, mapOptions);
		this.map.setTilt(45);

		//
		// Extra layers
		//
		this.trafficLayer = new google.maps.TrafficLayer();
		this.weatherLayer = new google.maps.weather.WeatherLayer({
			temperatureUnits: google.maps.weather.TemperatureUnit.FAHRENHEIT
		});
		if (this.state.layer.t) {
			this.trafficLayer.setMap(this.map);
			// add a timer updating the traffic tiles: 60sec
			this.trafficTimer = setInterval(this.trafficCB, 60*1000);
		}
		else
			this.trafficLayer.setMap(null);
		if (this.state.layer.w)
			this.weatherLayer.setMap(this.map);
		else
			this.weatherLayer.setMap(null);
	},

	load: function(state, date) {
		if (state) {
			this.state.mapType   = state.mapType;
			this.state.zoomLevel = state.zoomLevel;
			this.state.center    = state.center;
			this.state.layer     = state.layer;
		}
	},

	draw: function(date) {
	},

	move: function(date) {
		this.resize(date);
	},

	resize: function(date) {
		var localX      = this.sage2_x - ui.offsetX;
		var localY      = this.sage2_y - ui.offsetY;
		var localRight  = localX + this.sage2_width;
		var localBottom = localY + this.sage2_height;
		var viewX       = Math.max(localX, 0);
		var viewY       = Math.max(localY, 0);
		var viewRight   = Math.min(localRight,  ui.json_cfg.resolution.width);
		var viewBottom  = Math.min(localBottom, ui.json_cfg.resolution.height);
		var localWidth  = viewRight  - viewX;
		var localHeight = viewBottom - viewY;

		console.log(this.sage2_x, this.sage2_y, this.sage2_width, this.sage2_height);
		console.log(localX, localY, localRight, localBottom);
		console.log(viewX, viewY, viewRight, viewBottom, localWidth, localHeight);
		console.log(viewX-localX, viewY-localY);

		// completely off-screen
		if(localWidth <= 0 || localHeight <= 0) {
			this.div.style.width  = "1px";
			this.div.style.height = "1px";
		}
		else {
			this.div.style.width  = localWidth  + "px";
			this.div.style.height = localHeight + "px";
			this.div.style.left   = (viewX-localX) + "px";
			this.div.style.top    = (viewY-localY) + "px";
		}
//		console.log('Offset', (viewX-localX), -1*(viewY-localY));
//		this.map.panBy((viewX-localX),-1*(viewY-localY));
//		this.updateCenter();
//		this.map.setCenter(results[0].geometry.location);
 		this.map.setCenter(this.state.center);
		this.map.panBy((viewX-localX),(viewY-localY));
		
		//google.maps.event.trigger(this.map, 'resize');
		this.refresh(date);
	},

	updateCenter: function () {
		var c = this.map.getCenter();
		this.state.center = {lat:c.lat(), lng:c.lng()};

	},

	updateLayers: function () {
		// to trigger an 'oberve' event, need to rebuild the layer field
		this.state.layer = {w: this.weatherLayer.getMap() != null,
							t: this.trafficLayer.getMap() != null};
	},

	reloadTiles: function() {
		// Get the image tiles in the maps
		var tiles = this.div.getElementsByTagName('img');
		for (var i = 0; i < tiles.length; i++) {
			// get the URL
			var src = tiles[i].src;
			if (/googleapis.com\/vt\?pb=/.test(src)) {
				// add a date inthe URL will trigger a reload
				var new_src = src.split("&ts")[0] + '&ts=' + (new Date()).getTime();
				tiles[i].src = new_src;
			}
		}
	},

	quit: function() {
		// Make sure to delete the timer when quitting the app
		if (this.trafficTimer) clearInterval(this.trafficTimer);
	},

	event: function(eventType, position, user_id, data, date) {
		//console.log("Googlemaps event", eventType, position, user_id, data, date);
		var z;

		if (eventType === "pointerPress" && (data.button === "left")) {
			this.dragging = true;
			this.position.x = position.x;
			this.position.y = position.y;

			this.refresh(date);
		}
		else if (eventType === "pointerMove" && this.dragging) {
			this.map.panBy(this.position.x-position.x, this.position.y-position.y);
			this.updateCenter();
			this.position.x = position.x;
			this.position.y = position.y;

			this.refresh(date);
		}
		else if (eventType === "pointerRelease" && (data.button === "left")) {
			this.dragging = false;
			this.position.x = position.x;
			this.position.y = position.y;

			this.refresh(date);
		}

		// Scroll events for zoom
		else if (eventType === "pointerScroll") {
			this.scrollAmount += data.wheelDelta;

			if (this.scrollAmount >= 64) {
				// zoom out
				z = this.map.getZoom();
				this.map.setZoom(z-1);
				this.state.zoomLevel = this.map.getZoom();
				this.lastZoom = date;

				this.scrollAmount -= 64;
			}
			else if (this.scrollAmount <= -64) {
				// zoom in
				z = this.map.getZoom();
				this.map.setZoom(z+1);
				this.state.zoomLevel = this.map.getZoom();
				this.lastZoom = date;

				this.scrollAmount += 64;
			}
			this.resize(date);
			this.refresh(date);
		}
		else if (eventType === "widgetEvent"){
			switch(data.ctrlId){
				case "Map":
					this.changeMapType();
					break;
				case "Weather":
					this.toggleWeather();
					break;
				case "Traffic":
					this.toggleTraffic();
					break;
				case "ZoomIn":
					this.relativeZoom(1);
					break;
				case "ZoomOut":
					this.relativeZoom(-1);
					break;
				case "Zoom":
					switch (data.action){
						case "sliderLock":
							break;
						case "sliderUpdate":
							break;
						case "sliderRelease":
							this.map.setZoom(this.state.zoomLevel);
							break;
						default:
							console.log("No handler for: "+ data.ctrlId + "->" + data.action);
							break;
					}
					break;
				case "Address":
					this.codeAddress(data.text);
					this.updateCenter();
					this.map.setZoom(15);
					this.state.zoomLevel = this.map.getZoom();
					break;
				default:
					console.log("No handler for:", data.ctrlId);
			}
		}
		else if (eventType === "keyboard") {
			if(data.character === "m") {
				// change map type
				this.changeMapType();
			}
			else if (data.character === "t") {
				this.toggleTraffic();
			}
			else if (data.character === "w") {
				// add/remove weather layer
				this.toggleWeather();
			}

			this.refresh(date);
		}

		else if (eventType === "specialKey") {
			if (data.code === 18 && data.state === "down") {      // alt
				// zoom in
				this.relativeZoom(1);
			}
			else if (data.code === 17 && data.state === "down") { // control
				// zoom out
				this.relativeZoom(-1);
			}
			else if (data.code === 37 && data.state === "down") { // left
				this.map.panBy(-100, 0);
				this.updateCenter();
			}
			else if (data.code === 38 && data.state === "down") { // up
				this.map.panBy(0, -100);
				this.updateCenter();
			}
			else if (data.code === 39 && data.state === "down") { // right
				this.map.panBy(100, 0);
				this.updateCenter();
			}
			else if (data.code === 40 && data.state === "down") { // down
				this.map.panBy(0, 100);
				this.updateCenter();
			}

			this.refresh(date);
		}
	},
	changeMapType: function(){
		if (this.state.mapType === google.maps.MapTypeId.TERRAIN)
			this.state.mapType = google.maps.MapTypeId.ROADMAP;
		else if (this.state.mapType === google.maps.MapTypeId.ROADMAP)
			this.state.mapType = google.maps.MapTypeId.SATELLITE;
		else if (this.state.mapType === google.maps.MapTypeId.SATELLITE)
			this.state.mapType = google.maps.MapTypeId.HYBRID;
		else if (this.state.mapType === google.maps.MapTypeId.HYBRID)
			this.state.mapType = google.maps.MapTypeId.TERRAIN;
		else
			this.state.mapType = google.maps.MapTypeId.HYBRID;
		this.map.setMapTypeId(this.state.mapType);
	},
	toggleWeather: function() {
		if (this.weatherLayer.getMap() == null)
			this.weatherLayer.setMap(this.map);
		else
			this.weatherLayer.setMap(null);
		this.updateLayers();
	},
	toggleTraffic: function() {
		// add/remove traffic layer
		if (this.trafficLayer.getMap() == null) {
			this.trafficLayer.setMap(this.map);
			// add a timer updating the traffic tiles: 60sec
			this.trafficTimer = setInterval(this.trafficCB, 60*1000);
		}
		else {
			this.trafficLayer.setMap(null);
			// remove the timer updating the traffic tiles
			clearInterval(this.trafficTimer);
		}
		this.updateLayers();
	},
	relativeZoom: function(delta){
		delta = parseInt(delta);
		delta = (delta > -1)? 1 : -1;
		var z = this.map.getZoom();
		this.map.setZoom(z+delta);
		this.state.zoomLevel = this.map.getZoom();
	},
	codeAddress: function(text) {
		this.geocoder.geocode( { 'address': text}, function(results, status) {
			if (status === google.maps.GeocoderStatus.OK) {
				this.map.setCenter(results[0].geometry.location);
				/*var marker = new google.maps.Marker({
				map: this.map,
				position: results[0].geometry.location
				});*/
			} else {
				console.log('Geocode was not successful for the following reason: ' + status);
			}
		}.bind(this));
	}

});
