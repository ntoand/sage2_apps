//
// SAGE2 application: div
// by: luc renambot <renambot@gmail.com>
//
// Copyright (c) 2015
//

var scale_div = SAGE2_App.extend( {
	init: function(data) {
		// Create div into the DOM
		this.SAGE2Init("div", data);
		// Set the background to black
		this.element.style.backgroundColor = 'black';

		// move and resize callbacks
		this.resizeEvents = "continuous";
		this.moveEvents   = "continuous";

		// Create a new div
		this.newdiv = document.createElement("div");
		this.newdiv.style.position   = "absolute";
		this.newdiv.style.top        = "0";
		this.newdiv.style.left       = "0";
		this.newdiv.style.width      = "100%";
		this.newdiv.style.height     = "100%";

		// Add a paragraph
		var para = document.createElement("p");
		para.style.color      = "white";
		para.style.position   = "absolute";
		para.style.textAlign  = "center";
		// no wrapping text
		para.style.whiteSpace = "nowrap";
		// Paragraph in the center
		para.style.top        = "50%";
		para.style.left       = "50%";
		// Make it big
		para.style.fontSize   = 10 * this.config.ui.titleTextSize + "px";
		var node = document.createTextNode("NEW");
		// Add everything to the DOM
		para.appendChild(node);
		this.newdiv.appendChild(para);
		this.element.appendChild(this.newdiv);

		// Set center of scale
		this.newdiv.style.transformOrigin = "50% 50%";
		this.newdiv.style.transform = "scale(1)";

		// SAGE2 Application Settings
		//
		// Control the frame rate for an animation application
		this.maxFPS = 2.0;
		// Not adding controls but making the default buttons available
		this.controls.finishedAddingControls();
		this.enableControls = true;
	},

	load: function(date) {
		console.log('div> Load with state value', this.state.value);
		this.refresh(date);
	},

	draw: function(date) {
		// draw
	},

	resize: function(date) {
		// The 800 values comes from the initial size in instructions.json
		console.log('div> Draw with scale value', this.sage2_width / 800);
		this.newdiv.style.transform = "scale(" + (this.sage2_width / 800) + ")";
		this.refresh(date);
	},
	move: function(date) {
		this.refresh(date);
	},

	quit: function() {
		// Make sure to delete stuff (timers, ...)
	},

	event: function(eventType, position, user_id, data, date) {
		if (eventType === "pointerPress" && (data.button === "left")) {
		}
		else if (eventType === "pointerMove" && this.dragging) {
		}
		else if (eventType === "pointerRelease" && (data.button === "left")) {
		}

		// Scroll events for zoom
		else if (eventType === "pointerScroll") {
		}
		else if (eventType === "widgetEvent"){
		}
		else if (eventType === "keyboard") {
			if (data.character === "m") {
				this.refresh(date);
			}
		}
		else if (eventType === "specialKey") {
			if (data.code === 37 && data.state === "down") { // left
				this.refresh(date);
			}
			else if (data.code === 38 && data.state === "down") { // up
				this.refresh(date);
			}
			else if (data.code === 39 && data.state === "down") { // right
				this.refresh(date);
			}
			else if (data.code === 40 && data.state === "down") { // down
				this.refresh(date);
			}
		}
	}
});
