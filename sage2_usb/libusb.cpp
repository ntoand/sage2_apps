#include "libusb.h"

#include <iostream>
#include <fstream>
#include <cstdio>
#include <stdlib.h>

#include "websocketIO.h"

// function prototypes
void ws_open(websocketIO* ws);
void ws_initialize(websocketIO* ws, std::string data);
void ws_displayConfig(websocketIO* ws, std::string data);

std::string uniqueID;
websocketIO* wsio;
int ready = 0;

libusb *self;

libusb::libusb(const char *name, std::string configFile)
{
	self = this;

	_devList = NULL;
	_dev = NULL;
	_vendorID = -1;
	_productID = -1;
	_entryNum = 0;
	_configNum = 0;
	_numConfigs = 0;
	_interfaceNum = 0;
	_numInterfaces = 0;
	_altSettingNum = 0;
	_numAltSettings = 0;
	_endpointNum = 0;
	_numEndpoints = 0;
	_wheelOffset = 0;
	_withWheel = false;
	_wheelTimeout = 0.2;
	_address = 0;
	_packetSize = 0;
	_buttonOffsetSet = false;
	_buttonOffset = 0;
	_context = NULL;

	_withXY = false;
	_xOffset = -1;
	_yOffset = -1;
	_wheelIndex = 0;
	_xIndex = 0;
	_yIndex = 0;
	_xyTimeout = 0.05;

	_error = false;

	num_buttons = 0;
	num_channel = 0;
	_buttonLocal = NULL;
	_valLocal = NULL;

	_driverPresent = false;

	_url   = (char*)"wss://localhost:443";
	_label = (char*)"USB";

	if(!loadConfigFile(configFile))
	{
		_error = true;
		return;
	}

	if(_xOffset >= 0 && _yOffset >= 0)
	{
		_withXY = true;
	}

	if(libusb_init(&_context) < 0)
	{
		std::cerr << "Error libusb init." << std::endl;
		_error = true;
		return;
	} else {
		std::cerr << "Success libusb init." << std::endl;		
	}

	int devCount;

	if((devCount = libusb_get_device_list(_context, &_devList)) < 0 )
	{
		std::cerr << "Error getting device list .." << std::endl;
		_error = true;
		return;	
	} else {
		std::cerr << "Success libusb_get_device_list." << std::endl;		
		printDevices(_devList);
	}

	if(_vendorID < 0 || _productID < 0)
	{
		std::cerr << "Error: vendorID/productID must be set." << std::endl;
		std::cerr << "Printing list of usb devices:" << std::endl;
		printDevices(_devList);
		_error = true;
		return;
	}

	bool usberror = false;

	_dev = findDevice(_devList);
	if(!_dev)
	{
		printConfig();
		std::cerr << "Error: device with vendorID/productID with given entry not found." << std::endl;
		std::cerr << "Printing list of usb devices:" << std::endl;
		printDevices(_devList);
		_error = true;
		return;
	} else {
		std::cerr << "Found entry: " << _dev <<  std::endl;		
	}

	libusb_device_descriptor desc;
	if(libusb_get_device_descriptor(_dev,&desc) >= 0)
	{
		_numConfigs = (int)desc.bNumConfigurations;
		if(_configNum >= 0 && _configNum < _numConfigs)
		{
			libusb_config_descriptor * cdesc;
			if(libusb_get_config_descriptor(_dev,_configNum,&cdesc) >= 0)
			{
				_numInterfaces = (int)cdesc->bNumInterfaces;
				if(_interfaceNum >= 0 && _interfaceNum < _numInterfaces)
				{
					libusb_interface interface = cdesc->interface[_interfaceNum];
					_numAltSettings = (int)interface.num_altsetting;
					if(_altSettingNum >= 0 && _altSettingNum < _numAltSettings)
					{
						libusb_interface_descriptor idesc = interface.altsetting[_altSettingNum];
						_numEndpoints = (int)idesc.bNumEndpoints;
						if(_endpointNum >= 0 && _endpointNum < _numEndpoints)
						{
							libusb_endpoint_descriptor edesc = idesc.endpoint[_endpointNum];
							_address = (int)edesc.bEndpointAddress;
							_packetSize = (int)edesc.wMaxPacketSize;
						}
						else
						{
							std::cerr << "Error: Invalid endpoint number." << std::endl;
							usberror = true;
						}
					}
					else
					{
						std::cerr << "Error: Invalid altSetting number." << std::endl;
						usberror = true;
					}
				}
				else
				{
					std::cerr << "Error: Invalid interface Number." << _interfaceNum << ":" << _numInterfaces << std::endl;
					usberror == true;
				}
			}
			else
			{
				std::cerr << "Error: unable to get config descriptor." << std::endl;
				usberror = true;
			}
		}
		else
		{
			std::cerr << "Error: Invalid config number." << std::endl;
			usberror = true;
		}
	}
	else
	{
		std::cerr << "Error: unable to get device descriptor." << std::endl;
		usberror = true;
	}

	printConfig();

	if(libusb_open(_dev, &_handle) < 0)
	{
		std::cerr << "Error opening device bus: " << libusb_get_bus_number(_dev) << " device: " << libusb_get_device_address(_dev) << std::endl;
		usberror = true;
	}

	if(usberror)
	{
		_error = true;
		return;
	}

	for(int i = 0; i < num_buttons; i++)
	{
		buttons[i] = 0;
		lastbuttons[i] = 0;
	}

	if(!num_buttons)
	{
		std::cerr << "Error: Number of Buttons is 0." << std::endl;
		_error = true;
	}
	else
	{
		_buttonLocal = new int[num_buttons];
		for(int i = 0; i < num_buttons; i++)
		{
			_buttonLocal[i] = 0;
		}
	}

	int numChannels = 0;
	int nextIndex = 0;

	if(_withWheel)
	{
		numChannels++;
		_wheelIndex = nextIndex;
		nextIndex++;
	}

	if(_withXY)
	{
		numChannels += 2;
		_xIndex = nextIndex;
		nextIndex++;
		_yIndex = nextIndex;
		nextIndex++;
	}

	if(numChannels)
	{
		num_channel = numChannels;
		_valLocal = new float[numChannels];
		for(int i = 0; i < numChannels; i++)
		{
			_valLocal[i] = 0.0;
			channel[i] = 0.0;
			last[i] = 0.0;
		}
	}

	_packet = new unsigned char[_packetSize];
	for(int i = 0; i < _numInterfaces; i++)
	{
		if(libusb_kernel_driver_active(_handle, i) == 1)
		{
			_driverPresent = true;

			if(libusb_detach_kernel_driver(_handle, i) != 0)
			{
				std::cerr << "Error detaching driver from interface " << i << std::endl;
			}
		}
		else
		{
			_driverPresent = false;
		}
	}

	if(libusb_claim_interface(_handle, _interfaceNum) != 0)
	{
		std::cerr << "Error: could not claim device interface." << std::endl;
		_error = true;
		return;
	}

	_transfer = libusb_alloc_transfer(5);
	libusb_fill_interrupt_transfer(_transfer, _handle, _address, _packet, _packetSize, transCallback, (void*)this, 10000);
	libusb_submit_transfer(_transfer);

	pthread_attr_t attr;
	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);

	_ti = new ThreadInit;
	_ti->context = _context;
	_ti->quit = false;
	pthread_mutex_init(&_ti->_quitLock, NULL);
	pthread_create(&_updateThread,&attr,libusbUpdate,(void*)_ti);

	pthread_attr_destroy(&attr);

	// SAGE2
	///////////////////////////////
	wsio = new websocketIO();
	wsio->openCallback(ws_open);
	wsio->on("initialize", ws_initialize);
	wsio->on("setupDisplayConfiguration", ws_displayConfig);
	std::string ws_uri = _url;
	wsio->run(ws_uri);
	///////////////////////////////



	std::cerr << "Init good." << std::endl;
}

libusb::~libusb()
{
	if(_devList)
	{
		libusb_free_device_list(_devList, 1);
	}
	if(_context)
	{
		libusb_exit(_context);
	}
}

bool libusb::isError()
{
	return _error;
}

void libusb::mainloop()
{
	struct timeval current_time;

	//server_mainloop();
	//std::cerr << "libusb::mainloop " << std::endl;

	gettimeofday(&current_time, NULL);
	timestamp = current_time;

	pthread_mutex_lock(&_updateLock);

	if(_withWheel)
	{
		if(_valLocal[_wheelIndex] != 0.0)
		{
			double timeDif = (current_time.tv_sec - _lastUpdateTime.tv_sec) + ((current_time.tv_usec - _lastUpdateTime.tv_usec) / 1000000.0);
	    	//std::cerr << "Time diff: " << timeDif << std::endl;
			if(timeDif > _wheelTimeout)
			{
				_valLocal[_wheelIndex] = 0.0;
				//std::cerr << "Time reset: " << timeDif << std::endl;
			} else {
				std::cerr << "wheel before: " << _valLocal[_wheelIndex] << std::endl;
				double newval;
				if (_valLocal[_wheelIndex] > 0) {
					newval = 0.5 / timeDif;
					if (newval > 128.0) newval = 128.0;
				} else {
					newval = -0.5 / timeDif;
					if (newval < -128.0) newval = -128.0;
				}
				std::cerr << "\twheel: " << newval << std::endl;
				_valLocal[_wheelIndex] = newval;				
			}
		}
		channel[_wheelIndex] = _valLocal[_wheelIndex];

		if (channel[_wheelIndex] != 0) {
			//std::cerr << "Wheel: " << channel[_wheelIndex] << std::endl;
			if (ready) {
				boost::property_tree::ptree state;
				wsio->emit("pointerScrollStart", state);
				boost::property_tree::ptree state1;
				state1.put<int>("wheelDelta", channel[_wheelIndex]);
				wsio->emit("pointerScroll", state1);
			}
		}
	}

	if(_withXY)
	{
		if(_valLocal[_xIndex] != 0.0)
		{
			double timeDif = (current_time.tv_sec - _lastUpdateTime.tv_sec) + ((current_time.tv_usec - _lastUpdateTime.tv_usec) / 1000000.0);
			//std::cerr << "Time diff: " << timeDif << std::endl;
			if(timeDif > _xyTimeout)
			{
				_valLocal[_xIndex] = 0.0;
			}
		}
		channel[_xIndex] = _valLocal[_xIndex];

		if(_valLocal[_yIndex] != 0.0)
		{
			double timeDif = (current_time.tv_sec - _lastUpdateTime.tv_sec) + ((current_time.tv_usec - _lastUpdateTime.tv_usec) / 1000000.0);
			//std::cerr << "Time diff: " << timeDif << std::endl;
			if(timeDif > _xyTimeout)
			{
				_valLocal[_yIndex] = 0.0;
			}
		}
		channel[_yIndex] = _valLocal[_yIndex];

		//if (channel[_xIndex]!=0.0) std::cerr << "channel X: " << channel[_xIndex] << std::endl;
		//if (channel[_yIndex]!=0.0) std::cerr << "channel Y: " << channel[_yIndex] << std::endl;

		if (channel[_xIndex]!=0.0 || channel[_yIndex]!=0.0) {
			// {deltaX: Math.round(movementX*this.sensitivity),
			//   deltaY: Math.round(movementY*this.sensitivity)});	
		
			// Send delta position
			if (ready) {
				boost::property_tree::ptree state;
				state.put<int>("deltaX", channel[_xIndex] * _hfactor);
				state.put<int>("deltaY", channel[_yIndex] * _vfactor);
				wsio->emit("pointerMove", state);
			}
		}


	}

	static int buttonPress = 0;
	static struct timeval buttonPressTime;

	for(int i = 0; i < num_buttons; i++)
	{
		if (buttons[i] != _buttonLocal[i]) {
			//std::cerr << "button " << i << ":" << static_cast<int>( _buttonLocal[i] ) << std::endl;
			if (_buttonLocal[i] == 1) {
				boost::property_tree::ptree state;
				std::string btn = (i == 0) ? "left" : (i == 2) ? "middle" : "right";
				state.put<std::string>("button", btn);
				wsio->emit("pointerPress", state);

				static struct timeval now;
				gettimeofday(&now, NULL);
				double isClick = (now.tv_sec - buttonPressTime.tv_sec) + ((now.tv_usec - buttonPressTime.tv_usec) / 1000000.0);
				if (buttonPress && (isClick<0.35)) {
					state.put<std::string>("button", btn);
					//std::cerr << "Double CLICK " << std::endl;
					wsio->emit("pointerDblClick", state);
					buttonPress = 0;
				} else {
					//std::cerr << "NOT Double CLICK " << isClick << std::endl;
					buttonPress = 1;
				}
				buttonPressTime = now;
			} else {
				boost::property_tree::ptree state;
				std::string btn = (i == 0) ? "left" : (i == 2) ? "middle" : "right";
				state.put<std::string>("button", btn);
				wsio->emit("pointerRelease", state);

				//buttonPress = 0;
			}
		}
		buttons[i] = _buttonLocal[i];

	}

	pthread_mutex_unlock(&_updateLock);

	//report_changes();

	struct timespec ts;
	ts.tv_sec = 0;
	//ts.tv_nsec = 14000000;  // 74Hz ???
	ts.tv_nsec = 10000000;  // 100Hz
	nanosleep(&ts,NULL);
}

bool libusb::loadConfigFile(std::string & configFile)
{
	char alabel[256];
	memset(alabel, 0, 256);
	std::ifstream infile(configFile.c_str());
	std::string line;
	if(infile.is_open())
	{
		while(infile.good())
		{
			std::getline(infile,line);
			int value;
			float fvalue;
			if(sscanf(line.c_str(),"numbuttons %d",&value) == 1)
			{
				num_buttons = value;
			}
			else if(sscanf(line.c_str(),"vendorid %d",&value) == 1)
			{
				_vendorID = value;
			}
			else if(sscanf(line.c_str(),"productid %d",&value) == 1)
			{
				_productID = value;
			}
			else if(sscanf(line.c_str(),"entry %d",&value) == 1)
			{
				_entryNum = value;
			}
			else if(sscanf(line.c_str(),"config %d",&value) == 1)
			{
				_configNum = value;
			}
			else if(sscanf(line.c_str(),"interface %d",&value) == 1)
			{
				_interfaceNum = value;
			}
			else if(sscanf(line.c_str(),"altsetting %d",&value) == 1)
			{
				_altSettingNum = value;
			}
			else if(sscanf(line.c_str(),"endpoint %d",&value) == 1)
			{
				_endpointNum = value;
			}
			else if(sscanf(line.c_str(),"buttonoffset %d",&value) == 1)
			{
				_buttonOffsetSet = true;
				_buttonOffset = value;
			}
			else if(sscanf(line.c_str(),"xoffset %d",&value) == 1)
			{
				_xOffset = value;
			}
			else if(sscanf(line.c_str(),"yoffset %d",&value) == 1)
			{
				_yOffset = value;
			}
			else if(sscanf(line.c_str(),"wheeloffset %d",&value) == 1)
			{
				_withWheel = true;
				_wheelOffset = value;
			}
			else if(sscanf(line.c_str(),"wheeltimeout %f",&fvalue) == 1)
			{
				_wheelTimeout = fvalue;
			}
			else if(sscanf(line.c_str(),"label %s",alabel) == 1)
			{
				std::cerr << "Label: " << alabel << std::endl;
				_label = strdup(alabel);
			}
			else if(sscanf(line.c_str(),"url %s",alabel) == 1)
			{
				std::cerr << "URL: " << alabel << std::endl;
				_url = strdup(alabel);
			}
		}
	}
	else
	{
		std::cerr << "Unable to open config file: " << configFile << std::endl;
		return false;
	}

	return true;
}

void libusb::printDevices(libusb_device ** list)
{
	libusb_device * dev;

	int i = 0;

	while((dev = list[i++]) != NULL)
	{
		libusb_device_descriptor desc;
		if(libusb_get_device_descriptor(dev, &desc) < 0)
		{
			std::cerr << "Error getting device descriptor for usb device " << i-1 << std::endl;
			continue;
		}

		libusb_device_handle * handle;
		if(libusb_open(dev, &handle) < 0)
		{
			std::cerr << "Error opening device " << i-1 << std::endl;
			continue;
		}

		unsigned char buffer[255];

		std::cerr << "Device: " << i-1 << std::endl;

		if(desc.iManufacturer)
		{
			libusb_get_string_descriptor_ascii(handle, desc.iManufacturer, buffer, 255);
			std::cerr << "Manufacturer: " << buffer  << std::endl;
		}

		if(desc.iProduct)
		{
			libusb_get_string_descriptor_ascii(handle, desc.iProduct, buffer, 255);
			std::cerr << "Product: " << buffer  << std::endl;
		}

		if(desc.iSerialNumber)
		{
			libusb_get_string_descriptor_ascii(handle, desc.iSerialNumber, buffer, 255);
			std::cerr << "Serial Number: " << buffer  << std::endl;
		}

		std::cerr << "VendorID: " << desc.idVendor << std::endl;
		std::cerr << "ProductID: " << desc.idProduct << std::endl;
		std::cerr << std::endl;

		libusb_close(handle);
	} 
}

libusb_device * libusb::findDevice(libusb_device ** list)
{
	libusb_device * dev;

	int i = 0;

	int entry = 0;

	while((dev = list[i++]) != NULL)
	{
		libusb_device_descriptor desc;
		if(libusb_get_device_descriptor(dev, &desc) < 0)
		{
			std::cerr << "Error getting device descriptor for usb device " << i-1 << std::endl;
			continue;
		}


		if(_vendorID == desc.idVendor && _productID == desc.idProduct && entry >= _entryNum)
		{
			std::cerr << "Found it" << std::endl;
			return dev;
		}
		else if(_vendorID == desc.idVendor && _productID == desc.idProduct)
		{
			std::cerr << "Skipping it" << std::endl;
			entry++;
		} else {
			std::cerr << i-1 << ": not it - vendor: " << desc.idVendor << " - product: " << desc.idProduct << std::endl;
		}
	}
	return NULL;
}

void libusb::printConfig()
{
	std::cerr << "-------------------------" << std::endl;
	std::cerr << "Config Values:" << std::endl;
	std::cerr << "VendorID: " << _vendorID << std::endl;
	std::cerr << "ProductID: " << _productID << std::endl;
	std::cerr << "Entry Number: " << _entryNum << std::endl;
	std::cerr << "Config Number: " << _configNum << std::endl;
	std::cerr << "Number of Configs: " << _numConfigs << std::endl;
	std::cerr << "Interface Number: " << _interfaceNum << std::endl;
	std::cerr << "Number of Interfaces: " << _numInterfaces << std::endl;
	std::cerr << "Alt Setting Number: " << _altSettingNum << std::endl;
	std::cerr << "Number of Alt Settings: " << _numAltSettings << std::endl;
	std::cerr << "Endpoint Number: " << _endpointNum << std::endl;
	std::cerr << "Number of Endpoints: " << _numEndpoints << std::endl;
	std::cerr << "Endpoint Address: " << _address << std::endl;
	std::cerr << "Button Offset: " << _buttonOffset << std::endl;
	std::cerr << "X Offset: " << _xOffset << std::endl;
	std::cerr << "Y Offset: " << _yOffset << std::endl;
	std::cerr << "Packet Size: " << _packetSize << std::endl;
	std::cerr << "SAGE2 Label: " << _label << std::endl;
	std::cerr << "SAGE2 URL: " << _url << std::endl;
	if(_withWheel)
	{
		std::cerr << "Wheel Offset: " << _wheelOffset << std::endl;
		std::cerr << "Wheel Timeout (sec): " << _wheelTimeout << std::endl;
	}
	std::cerr << "-------------------------" << std::endl;
}

void transCallback(struct libusb_transfer * transfer)
{
	// std::cerr << "Callback." << std::endl;
	// std::cerr << "Status: " << transfer->status << std::endl;
	if(transfer->status == LIBUSB_TRANSFER_TIMED_OUT) {
		//std::cerr << "Timeout" << std::endl;
	}
	else if(transfer->status == LIBUSB_TRANSFER_COMPLETED)
	{
		//std::cerr << "Got valid transfer. length: " << transfer->actual_length << std::endl;
		libusb * mc = (libusb*)transfer->user_data;

		if(!mc->_buttonOffsetSet)
		{
			if(!transfer->actual_length)
			{
				std::cerr << "No bytes read." << std::endl;
			}
			else
			{
				std::cerr << "Packet: ";
				for(int i = 0; i < mc->_packetSize; i++)
				{
					std::cerr << (int)(transfer->buffer)[i] << " ";
				}
				std::cerr << std::endl;
			}
		}
		else if(transfer->actual_length)
		{
			pthread_mutex_lock(&mc->_updateLock);
			unsigned int mask = (unsigned int)(transfer->buffer)[mc->_buttonOffset];
			//std::cerr << "Button Mask: " << mask << std::endl;
			unsigned int byte = 1;
			for(int i = 0; i < mc->getNumButtons(); i++)
			{
				if(mask & byte)
				{
					//std::cerr << "Button " << i << " to 1" << std::endl;
					mc->_buttonLocal[i] = 1;
				}
				else
				{
					//std::cerr << "Button " << i << " to 0" << std::endl;
					mc->_buttonLocal[i] = 0;
				}
				byte = byte << 1;
			}

			if(mc->_withWheel)
			{
				float val = (float)((char)(transfer->buffer)[mc->_wheelOffset]);
				if(val >= 1.0)
				{
					val = 1.0;
				}
				else if(val <= -1.0)
				{
					val = -1.0;
				}
				mc->_valLocal[mc->_wheelIndex] = val;
			}

			if(mc->_withXY)
			{
				mc->_valLocal[mc->_xIndex] = (float)((char)(transfer->buffer)[mc->_xOffset]);
				mc->_valLocal[mc->_yIndex] = (float)((char)(transfer->buffer)[mc->_yOffset]);
			}

			gettimeofday(&mc->_lastUpdateTime,NULL);
			pthread_mutex_unlock(&mc->_updateLock);
		}
		else {
	  	  // std::cerr << "No Bytes read." << std::endl;
		}
	}

	libusb_submit_transfer(transfer);
}

void * libusbUpdate(void * init)
{
	struct ThreadInit * ti = (ThreadInit*)init;
	std::cerr << "Entering libusbUpdate: " << std::endl;
	while(1)
	{
		// std::cerr << "trying to lock" << std::endl;
		pthread_mutex_lock(&ti->_quitLock);
		if(ti->quit)
		{
			// std::cerr << "trying to unlock to quit" << std::endl;
			pthread_mutex_unlock(&ti->_quitLock);
			break;
		}
		// std::cerr << "trying to unlock" << std::endl;
		pthread_mutex_unlock(&ti->_quitLock);
		// std::cerr << "calling libusb_handle_events" << std::endl;
		libusb_handle_events(ti->context);
	}
	// std::cerr << "libusbUpdate done" << std::endl;
	pthread_exit(NULL);
}



/******** WebSocket Callback Functions ********/
void ws_open(websocketIO* ws) {
    // send addClient message
	boost::property_tree::ptree data;
	data.put<std::string>("clientType", "sagePointer");
	data.put<bool>("sendsPointerData", true);
	data.put<bool>("sendsMediaStreamFrames", false);
	data.put<bool>("receivesDisplayConfiguration", true);

	ws->emit("addClient", data);
}


void ws_displayConfig(websocketIO* ws, std::string data) {
	std::istringstream iss(data);
	boost::property_tree::ptree json_data;
	boost::property_tree::read_json(iss, json_data);
	int w = json_data.get<int> ("data.resolution.width");
	int h = json_data.get<int> ("data.resolution.height");
	int c = json_data.get<int> ("data.layout.columns");
	int r = json_data.get<int> ("data.layout.rows");
	double hfactor = ( w * c ) / 1920.0;
	double vfactor = ( h * r ) / 1080.0;
	std::cerr << "Mouse scaling factor: " << hfactor << " x  " << vfactor << std::endl;
	self->_hfactor = hfactor;
	self->_vfactor = vfactor;
}

void ws_initialize(websocketIO* ws, std::string data) {
	boost::property_tree::ptree json_data;
	std::istringstream iss(data);
	boost::property_tree::read_json(iss, json_data);

	uniqueID = json_data.get<std::string> ("data.UID");
	fprintf(stderr, "ID: %s\n", uniqueID.c_str());

	// Start the pointer	
	boost::property_tree::ptree state;
	state.put<std::string>("label", self->_label);
	state.put<std::string>("color", "#11b021");
	ws->emit("startSagePointer", state);

	ready = 1;
}

