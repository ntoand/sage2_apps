## SAGE2 API for external apps: sail2

C++ API for SAGE2

### First time
	* make setup
		* to install websocketpp library (using git)
		* or: git clone https://github.com/zaphoyd/websocketpp.git
	* make rgb
		* or: convert pattern.jpg pattern.rgb

### make
	* it builds library libsail.so
	* it builds testapp
	* 'make dox' to build the documentation (lacking)

### run
	* ./testapp hostname:port
		* ./testapp localhost:9292

