// SAGE2 is available for use under the SAGE2 Software License
//
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
// and University of Hawai'i at Manoa's Laboratory for Advanced Visualization and
// Applications (LAVA)
//
// See full text, terms and conditions in the LICENSE.txt included file
//
// Copyright (c) 2014


#include "sail2.h"


std::vector<std::string> split(std::string, char);

void ws_open(WebSocketIO*);
void ws_initialize(WebSocketIO*, boost::property_tree::ptree);
void ws_requestNextFrame(WebSocketIO*, boost::property_tree::ptree);
void ws_stopMediaCapture(WebSocketIO*, boost::property_tree::ptree);
void ws_setItemPositionAndSize(WebSocketIO*, boost::property_tree::ptree);
void ws_finishedResize(WebSocketIO*, boost::property_tree::ptree);
void ws_setupDisplayConfiguration(WebSocketIO*, boost::property_tree::ptree);


bool continuous_resize = true;

sail::sail(const char *appname, int ww, int hh, enum sagePixFmt pixelfmt, std::string hostname, float frate)
{
	ws_host     = hostname;
	width       = ww;
	height      = hh;
	format      = pixelfmt;
	frameRate   = frate;
	got_request = false;
	got_frame   = false;

	applicationName = std::string(appname);

	frame = (unsigned char*)malloc(getBufferSize());
	memset(frame, 0, getBufferSize());

	// Create the websocket for SAGE2
	std::string ws_uri = "ws://" + std::string(hostname);
	fprintf(stderr, "URI to SAGE2: %s\n", ws_uri.c_str());
	wsio = new WebSocketIO(ws_uri, this);
	wsio->open(ws_open);
}

sail::~sail()
{
	// Close the socket
	//wsio->close();
}

int sail::getBufferSize()
{
	if (format == PIXFMT_888)
		return (width*height*3);
	else if (format == PIXFMT_YUV)
		return (width*height*2);
	else return 0;
}

static int _initialized = 0;

sail* createSAIL(const char *appname, int ww, int hh,
	enum sagePixFmt pixelfmt, const char *url, float frate)
{
	sail *sageInf;

	// Initialize a few things
	if (! _initialized) {
		initTime();
		_initialized = 1;
	}

	// Allocate the sail object
	sageInf = new sail(appname, ww, hh, pixelfmt, url, frate);
	if (sageInf == NULL)
		return NULL;

	return sageInf;
}


void deleteSAIL(sail *sageInf)
{
	delete sageInf;
	exit(0);
}


std::string nextBuffer(sail *sageInf)
{
	unsigned char *ptr = (unsigned char*) sageInf->getBuffer();
	std::string jpegFrame = convertToJpeg(ptr, sageInf->getWidth(), sageInf->getHeight());

	size_t out;
	char* image_base64 = base64_encode((const unsigned char*)jpegFrame.c_str(), jpegFrame.length(), &out);

	std::string string_base64(image_base64, image_base64 + out);
	free(image_base64);

	return string_base64;
}

void swapWithBuffer(sail *sageInf, unsigned char *pixptr)
{
	// Swap the main sail object
	unsigned char *ptr = (unsigned char*) sageInf->getBuffer();
	memcpy(ptr, pixptr, sageInf->getBufferSize());
	sageInf->got_frame = true;
	swapBuffer(sageInf);
}

void swapBuffer(sail *sageInf)
{
	if (sageInf->got_request) {
		sageInf->got_request = false;
		sageInf->got_frame   = false;

		boost::property_tree::ptree state;
		state.put<std::string>("src",      nextBuffer(sageInf));
		state.put<std::string>("type",     "image/jpeg");
		state.put<std::string>("encoding", "base64");

		boost::property_tree::ptree emit_data;
		emit_data.put<std::string>("id", sageInf->uniqueID + "|0");
		emit_data.put_child("state", state);
		sageInf->wsio->emit("updateMediaStreamFrame", emit_data);
	}
}


void processMessages(sail *sageInf)
{
	// Is the socket closed
	if (sageInf->wsio->isDone()) {
		exit(0);
	}

	// Not sure if needed
	sageInf->wsio->runOne();
}




/******** Helper Functions ********/
std::vector<std::string> split(std::string s, char delim) {
	std::stringstream ss(s);
	std::string item;
	std::vector<std::string> elems;
	while (std::getline(ss, item, delim)) {
		elems.push_back(item);
	}
	return elems;
}

/******** WebSocket Callback Functions ********/
void ws_open(WebSocketIO* ws) {
	sail *sageInf = (sail*) ws->getPointer();
	fprintf(stderr, "WEBSOCKET OPEN %s\n", sageInf->applicationName.c_str());

	ws->on("initialize", ws_initialize);
	ws->on("requestNextFrame", ws_requestNextFrame);
	ws->on("stopMediaCapture", ws_stopMediaCapture);
	ws->on("setupDisplayConfiguration", ws_setupDisplayConfiguration);

	boost::property_tree::ptree data;
	data.put<std::string>("clientType", sageInf->applicationName);
	boost::property_tree::ptree req;
	req.put<bool>("config",  true);
	req.put<bool>("version", false);
	req.put<bool>("time",    false);
	req.put<bool>("console", false);
	data.put_child("requests", req);

	ws->emit("addClient", data);
}

// {
//     name: "mysite",
//     host: "localhost",
//     port: 9090,
//     index_port: 9292,
//     resolution: {
//         width: 1920,
//         height: 1080
//     },
//     layout: {
//         rows: 1,
//         columns: 1
//     },
//     displays: [
//         {
//             row: 0,
//             column: 0
//         }
//     ],
//     totalWidth: 1920,
//     totalHeight: 1080,
// }

void ws_setupDisplayConfiguration(WebSocketIO*, boost::property_tree::ptree data) {
	std::string dname   = data.get<std::string>("name");
	int dwidth  = data.get<int>("resolution.width");
	int dheight = data.get<int>("resolution.height");
	fprintf(stderr, "Display> %s %d %d\n", dname.c_str(), dwidth, dheight);
}

void ws_initialize(WebSocketIO* ws, boost::property_tree::ptree data) {
	sail *sageInf = (sail*) ws->getPointer();
	sageInf->uniqueID = data.get<std::string> ("UID");
	fprintf(stderr, "SAGE2 ID: %s\n", sageInf->uniqueID.c_str());

	if (!sageInf->uniqueID.empty()) {
		fprintf(stderr, "Initializing connection\n");

		boost::property_tree::ptree emit_data;
		emit_data.put<std::string>("id",    sageInf->uniqueID + "|0");
		emit_data.put<std::string>("title", sageInf->applicationName);
		// black pixel 1x1 GIF
		emit_data.put<std::string>("src",      "R0lGODlhAQABAIAAAAAAAAAAACH5BAAAAAAALAAAAAABAAEAAAICTAEAOw==");
		emit_data.put<std::string>("type",     "image/gif");
		emit_data.put<std::string>("encoding", "base64");
		emit_data.put<int>("width",  sageInf->getWidth());
		emit_data.put<int>("height", sageInf->getHeight());
		ws->emit("startNewMediaStream", emit_data);
	}
}

void ws_requestNextFrame(WebSocketIO* ws, boost::property_tree::ptree data) {
	sail *sageInf = (sail*) ws->getPointer();
	fprintf(stderr, "ws_requestNextFrame\n");

	sageInf->got_request = true;
	if (sageInf->got_frame) {
		swapBuffer(sageInf);
	}
}

void ws_stopMediaCapture(WebSocketIO* ws, boost::property_tree::ptree data) {
	std::string streamId = data.get<std::string> ("streamId");
	int idx = atoi(streamId.c_str());
	fprintf(stderr, "STOP MEDIA CAPTURE: %d\n", idx);

	sail *sageInf = (sail*) ws->getPointer();
	deleteSAIL(sageInf);
}

void ws_setItemPositionAndSize(WebSocketIO* ws, boost::property_tree::ptree data) {
	sail *sageInf = (sail*) ws->getPointer();

   	// update browser window size during resize
	if (continuous_resize) {
		std::string id = data.get<std::string> ("elemId");
		std::vector<std::string> elemData = split(id, '|');
		std::string uid = "";
		int idx = -1;
		if (elemData.size() == 2){
			uid = elemData[0];
			idx = atoi(elemData[1].c_str());
		}

		if (uid == sageInf->uniqueID) {
			std::string w = data.get<std::string> ("elemWidth");
			std::string h = data.get<std::string> ("elemHeight");
			std::string x = data.get<std::string> ("elemLeft");
			std::string y = data.get<std::string> ("elemTop");
			float neww = atof(w.c_str());
			float newh = atof(h.c_str());
			float newx = atof(x.c_str());
			float newy = atof(y.c_str());
			printf("New pos & size: %.0f x %.0f - %.0f x %.0f\n", newx, newy, neww, newh);
		}
	}
}

void ws_finishedResize(WebSocketIO* ws, boost::property_tree::ptree data) {
	sail *sageInf = (sail*) ws->getPointer();

	std::string id = data.get<std::string> ("id");
	std::vector<std::string> elemData = split(id, '|');
	std::string uid = "";
	int idx = -1;
	if (elemData.size() == 2) {
		uid = elemData[0];
		idx = atoi(elemData[1].c_str());
	}

	if (uid == sageInf->uniqueID) {
		std::string w = data.get<std::string> ("elemWidth");
		std::string h = data.get<std::string> ("elemHeight");

		float neww = atof(w.c_str());
		float newh = atof(h.c_str());

		fprintf(stderr, "New size: %.0f x %.0f\n", neww, newh);
	}
}

