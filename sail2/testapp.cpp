// SAGE2 is available for use under the SAGE2 Software License
//
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
// and University of Hawai'i at Manoa's Laboratory for Advanced Visualization and
// Applications (LAVA)
//
// See full text, terms and conditions in the LICENSE.txt included file
//
// Copyright (c) 2014

#include "sail2.h"

//
// usage:  ./testapp hostname:port    (using the http port number)
//         ./testapp localhost:9292
//         ./testapp traoumad.evl.uic.edu
//

int
main(int argc, char **argv)
{
	fprintf(stderr, "Main: %s\n", argv[1]);

	sail *sageInf;

	// Read some uncompressed RGB data
	unsigned char *data = (unsigned char*)malloc(1920*1080*3);
	memset(data, 0, 1920*1080*3);
	FILE *f = fopen("pattern.rgb", "r");
	if (!f) {
		fprintf(stderr, "Cannot read [pattern.rgb]\n");
		exit(1);
	}
	fread(data, 1, 1920*1080*3, f);
	fclose(f);

	// Create a black frame
	unsigned char *data_black = (unsigned char*)malloc(1920*1080*3);
	memset(data_black, 0, 1920*1080*3);

	// Create SAIL2 object
	sageInf = createSAIL("testapp", 1920, 1080, PIXFMT_888, argv[1], 30);
	fprintf(stderr, "App created\n");

	int  count = 0;
	bool done  = false;

	double t1, t2, fps, interval, rate;
	rate = 5.0;
	interval = 1000000.0 / rate;

	while (! done) {
		t1 = getTime();

		count++;
		if (count>=100000) {
			done = true;
		}

		// Flip flop
		if (count%2)
			swapWithBuffer(sageInf, data);
		else
			swapWithBuffer(sageInf, data_black);

		processMessages(sageInf);

		// Wait a little
		t2 = getTime();
		fps = 1000000.0 / (t2-t1);
		if (fps > rate) {
			usleep( (1000000.0/rate) - (t2-t1)  );
		}
		// Check the actual rate
		t2 = getTime();
		fps = 1000000.0 / (t2-t1);
		fprintf(stderr, "\tFPS: %.2f\n", fps);
	}

	deleteSAIL(sageInf);
}

