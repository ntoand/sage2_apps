// SAGE2 is available for use under the SAGE2 Software License
//
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
// and University of Hawai'i at Manoa's Laboratory for Advanced Visualization and
// Applications (LAVA)
//
// See full text, terms and conditions in the LICENSE.txt included file
//
// Copyright (c) 2014-2015

// NPM packages
var program     = require('commander');           // parsing command-line arguments

// Local scripts
var vncClient   = require('./lib/node-vncclient');      // open a VNC connection
var websocketIO = require('./lib/node-websocket.io');   // creates WebSocket server and clients

/***************************************************************************************/

// Command line arguments
program
  .version('0.0.1')
  .option('-s, --server <value>',      'server hostname (127.0.0.1)', '127.0.0.1')
  .option('-p, --port <n>',            'port number (5900)', 5900, parseInt)
  .option('-c, --catchphrase <value>', 'password (secret)', 'secret')
  .option('-d, --display <value>',     'SAGE url (wss://localhost:443)', 'wss://localhost:443')
  .parse(process.argv);

console.log("VNC> ", program.server, program.port, program.catchphrase, program.display);

var client = new vncClient(program.server, program.port, program.catchphrase);
var ws     = null;
var ready  = false;

var elemWidth  = 0;
var elemHeight = 0;
var buttons    = 0;
var lastkey    = null;
var isShift = false;

//////////////////////////////////////////////////////////////////////////////////////
// From: https://github.com/kanaka/noVNC/blob/master/include/keyboard.js
//
// Given a keycode, try to predict which keysym it might be.
// If the keycode is unknown, null is returned.
function keysymFromKeyCode(keycode, shiftPressed) {
    if (typeof(keycode) !== 'number') {
        return null;
    }
    // won't be accurate for azerty
    if (keycode >= 0x30 && keycode <= 0x39) {
        return keycode; // digit
    }
    if (keycode >= 0x41 && keycode <= 0x5a) {
        // remap to lowercase unless shift is down
        return shiftPressed ? keycode : keycode + 32; // A-Z
    }
    if (keycode >= 0x60 && keycode <= 0x69) {
        return 0xffb0 + (keycode - 0x60); // numpad 0-9
    }

    switch(keycode) {
        case 0x20: return 0x20; // space
        case 0x6a: return 0xffaa; // multiply
        case 0x6b: return 0xffab; // add
        case 0x6c: return 0xffac; // separator
        case 0x6d: return 0xffad; // subtract
        case 0x6e: return 0xffae; // decimal
        case 0x6f: return 0xffaf; // divide
        case 0xbb: return 0x2b; // +
        case 0xbc: return 0x2c; // ,
        case 0xbd: return 0x2d; // -
        case 0xbe: return 0x2e; // .
    }
    
    return nonCharacterKey({keyCode: keycode});
}

// if the key is a known non-character key (any key which doesn't generate character data)
// return its keysym value. Otherwise return null
function nonCharacterKey(evt) {
    // evt.key not implemented yet
    if (!evt.keyCode) { return null; }
    var keycode = evt.keyCode;

    if (keycode >= 0x70 && keycode <= 0x87) {
        return 0xffbe + keycode - 0x70; // F1-F24
    }
    switch (keycode) {

        case 8  : return 0xFF08; // BACKSPACE
        case 13 : return 0xFF0D; // ENTER

        case 9  : return 0xFF09; // TAB

        case 27 : return 0xFF1B; // ESCAPE
        case 46 : return 0xFFFF; // DELETE

        case 36 : return 0xFF50; // HOME
        case 35 : return 0xFF57; // END
        case 33 : return 0xFF55; // PAGE_UP
        case 34 : return 0xFF56; // PAGE_DOWN
        case 45 : return 0xFF63; // INSERT

        case 37 : return 0xFF51; // LEFT
        case 38 : return 0xFF52; // UP
        case 39 : return 0xFF53; // RIGHT
        case 40 : return 0xFF54; // DOWN
        case 16 : return 0xFFE1; // SHIFT
        case 17 : return 0xFFE3; // CONTROL
        case 18 : return 0xFFE9; // Left ALT (Mac Option)

        case 224 : return 0xFE07; // Meta
        case 225 : return 0xFE03; // AltGr
        case 91  : return 0xFFEC; // Super_L (Win Key)
        case 92  : return 0xFFED; // Super_R (Win Key)
        case 93  : return 0xFF67; // Menu (Win Menu), Mac Command
        default: return null;
    }
}
//////////////////////////////////////////////////////////////////////////////////////

function opencb() {
	elemHeight = client.conn.height;
	elemWidth  = client.conn.width;

	console.log("Got my connection", elemWidth, elemHeight);


	//var wsURL = "wss://" + "localhost" + ":" + "443";
	var wsURL = program.display;
	ws = new websocketIO(wsURL, false, function() {
		console.log("websocket open: ", ws.remoteAddress);

                var clientDescription = {
                        clientType: "VNC",
                        requests: {"config":true,"version":false,"time":false,"console":false},
                        receivesWindowModification: true,
                        receivesInputEvents: true,
			sendsPointerData: false,
			sendsMediaStreamFrames: true,
			requestsServerFiles: false,
			sendsWebContentToLoad: true,
			launchesWebBrowser: false,
			sendsVideoSynchonization: false,
			sharesContentWithRemoteServer: false,
			receivesDisplayConfiguration: true,
			receivesClockTime: false,
			requiresFullApps: false,
			requiresAppPositionSizeTypeOnly: false,
			receivesMediaStreamFrames: false,
			receivesPointerData: true,
			receivesRemoteServerInfo: false
                };

		ws.emit('addClient', clientDescription);

		ws.on('initialize', function(wsio,data) {
			console.log("initialize:", data);
			client.id = data.UID+"|1"; 
			ws.emit('startNewMediaStream',
				{id:client.id,
				src:"R0lGODlhAQABAIAAAAAAAAAAACH5BAAAAAAALAAAAAABAAEAAAICTAEAOw==",
				type: "image/gif",
				encoding: "base64",
				title:"vnc "+program.server.toString()+":"+(program.port-5900).toString(),
				 width:client.conn.width, height:client.conn.height}
			);

		});

		ws.on('setupDisplayConfiguration', function(wsio,data) {
			console.log("setupDisplayConfiguration:", data.totalWidth, data.totalHeight);
		});
		
		ws.on('requestNextFrame', function(wsio,data) {
			ready = true;
		});

		ws.on('eventInItem', function(wsio,evt) {
			//console.log("eventInItem",evt);
			//console.log("clientid ",client.id);
                        if (evt.id!=client.id) {
			  console.log("evt.id==client.id");
                          return;
                        }
			if (evt.type === 'pointerMove') {
				client.pointerEvent(evt.position.x*client.conn.width/elemWidth,
				  evt.position.y*client.conn.height/elemHeight, buttons);
			}
			else if (evt.type === 'pointerPress') {
                         	console.log("pointerPress");
				var b = evt.data.button;
				if (b === 'left')  buttons = 1;
				if (b === 'right') buttons = 2;
				client.pointerEvent(evt.position.x*client.conn.width/elemWidth,
									evt.position.y*client.conn.height/elemHeight, buttons);
			}
			else if (evt.type === 'pointerRelease') {
                         	console.log("pointerRelease");
				buttons = 0;
				client.pointerEvent(evt.position.x*client.conn.width/elemWidth,
									evt.position.y*client.conn.height/elemHeight, buttons);
			}
			else if (evt.type === 'keyboard') {
				console.log("keyboard:", evt);
			}
			else if (evt.type === 'specialKey') {
				console.log("eventInItem:", evt);
				if (evt.data.code == 16) isShift = (evt.data.state=="down");
				client.keyEvent(keysymFromKeyCode(evt.data.code, isShift), evt.data.state=='up');
			} else {
				console.log("eventInItem:", evt);
			}
		});


		ws.on('setItemPositionAndSize', function(wsio,data) {
			elemWidth  = data.elemWidth;
			elemHeight = data.elemHeight;
		});

		ws.on('finishedResize', function(wsio,data) {
			//console.log("finishedResize");
		});

		ws.on('stopMediaStream', function(wsio,data) {
			console.log("stopMediaStream");
			process.exit(1);
		});
		ws.onclose(function() {
			console.log("Closed");
		});

	});
};

function framecb(image)
{
	if (ready) {
                //console.log('send frame');
		ws.emit('updateMediaStreamFrame', {
			id: client.id,
			state: {src: image.toString('binary'),
				type: "image/jpeg",
				encoding: "binary"}
		  }
		);
		ready = false;
	}
}

client.open( opencb , framecb );

